#include <include/ForexAnalyzer.h>
#include <include/SysLog.h>

ForexAnalyzer::ForexAnalyzer(json::JSON &cfg, std::shared_ptr<CurrencyAPI> currencyAPI) :
	_currencyAPI(currencyAPI), _killthread(true)
{
	_sleep = std::chrono::milliseconds(cfg.at("SleepBetweenCheckForNewData").ToInt());
	_maxDepth = cfg.at("MaxDepth").ToInt();
	if (_maxDepth > _nbCurrencies - 1)
		_maxDepth = _nbCurrencies - 1;

	_nbCurrencies = _currencyAPI->NbCurrencies();
	_threads.resize(_nbCurrencies);

	if (cfg.at("UseStdNumericThreshold").ToBool())
		_threshold = std::numeric_limits<double>::epsilon();
	else
		_threshold = cfg.at("UserDefinedThreshold").ToFloat();

	_initTransactionMap();
#ifndef NDEBUG
	_logSomeStats();
#endif // !NDEBIG

}

void ForexAnalyzer::_initTransactionMap() {
	// there is n currency that can be changed to n-1 other currency so the map is size (n-1)n
	for (auto its = _currencyAPI->Begin(); its != _currencyAPI->End(); ++its) {
		_currencies.push_back(its->first);
	}

	CurrencyAPI::SortVectorAlphabetically(_currencies);

	for (uint16_t i = 0; i < _nbCurrencies - 1;++i)
		for (uint16_t j = i + 1; j < _nbCurrencies;++j) {
			std::string transaction = _formatTransactionString(_currencies[i],_currencies[j]);
			EquivalentTransactions eqTrans = EquivalentTransactions(transaction);

			_equivalentRates.emplace_back(eqTrans);
		}
}

void ForexAnalyzer::Run() {
	_killthread = false;
	
	if (!_checkerThread.joinable())
		_checkerThread = std::thread(&ForexAnalyzer::_checkAndCompute, this);
}

void ForexAnalyzer::_checkAndCompute() {
	while (!_killthread) {

		while (!_currencyAPI->IsNewDataAvailable())
			std::this_thread::yield();

#ifndef NDEBUG
		SysLog::Log(SysLog::SeverityLevel::Debug, "New data available", true);
#endif // !NDEBUG

		_analyzeNewData();
	}
}

void ForexAnalyzer::_analyzeNewData() {
#ifndef NDEBUG
	auto start = std::chrono::high_resolution_clock::now();
#endif // !NDEBUG

	_processMap(_currencyAPI->GetMapCopy());

#ifndef NDEBUG
	auto end = std::chrono::high_resolution_clock::now();
	auto us = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	SysLog::Log(SysLog::SeverityLevel::Debug, "Data analysis duration of : " + std::to_string(us.count()) + " microseconds " + "for " + std::to_string(_nbCurrencies) + " currencies." );
#endif // !NDEBUG
}

void ForexAnalyzer::_processMap(RatesMap const &map) {
//TO DO: use a less retard approach
	_appendBasicRate(map);

	_processTransactions(map);

	for (auto &r : _equivalentRates) {
		if (_unequalEquivalentTransactions(r.Paths) != 0) {
			SysLog::Log("Unequal equivalent transaction found for " + r.transaction);
		}
	}
}

void ForexAnalyzer::_processAllTransactionPaths(EquivalentTransactions & eqTransaction, RatesMap const &Map) {

	std::string const &currentTransaction = eqTransaction.transaction;

	std::string const &baseCurrency = currentTransaction.substr(0, TickerSize);
	std::string const &targetCurrency = currentTransaction.substr(currentTransaction.size() - TickerSize);

#ifndef NDEBUG
	SysLog::Log(SysLog::SeverityLevel::Debug, "Processing paths for " + baseCurrency + " to " + targetCurrency);
#endif // !NDEBUG

	std::unordered_map<std::string, double> const &baseCurrencyMap = *Map.at(baseCurrency);

	std::vector<std::string> unprocessedCurrencies;

	uint8_t nbOtherCurrencies = (_nbCurrencies - 2);
	uint32_t nbPossiblitiesForThisTransaction = 1;

	for (uint16_t i = 0; i < _maxDepth; ++i)
		nbPossiblitiesForThisTransaction *= (nbOtherCurrencies - i);

	//TODO all that crap should be done at system startup
	for (const auto &r : baseCurrencyMap)
		if (r.first != targetCurrency)[[likely]] {
			Path p(baseCurrency);
			p.AddTransaction(r.first, r.second);
			for (uint16_t i = 0; i < (uint16_t)nbPossiblitiesForThisTransaction/nbOtherCurrencies; ++i)
				eqTransaction.AddPath(p);
			unprocessedCurrencies.emplace_back(r.first);
		}

	uint16_t currentDepth = 0;

	_fillPaths(eqTransaction, (uint16_t)nbPossiblitiesForThisTransaction / nbOtherCurrencies);

	//Appending target currency if not already the last 
	for (auto &r : eqTransaction.Paths) {
		std::string & lastCurrency = r.fullPath.back();
		if (lastCurrency != targetCurrency) {
			auto &lastCurrencyMap = Map.at(lastCurrency);
			double valueAtTarget = lastCurrencyMap->at(targetCurrency);
			r.AddTransaction(targetCurrency, valueAtTarget);
		}

	}
}

void ForexAnalyzer::_fillPaths(EquivalentTransactions &eqTransac, uint16_t eqPerGroup) {

	for (uint16_t i = 0; i < eqPerGroup  ; ++i) {
		//_recursiveTransaction();
	}
}

void ForexAnalyzer::_recursiveTransaction(Path &paths) {
	uint8_t currentDepth = 0;

	while (currentDepth < _maxDepth)
	{
		++currentDepth;
	}
}

void ForexAnalyzer::_processTransactions(RatesMap const &map) {

	for (auto &r : _equivalentRates)
		_processAllTransactionPaths(r, map);
}

void ForexAnalyzer::_appendBasicRate(RatesMap const &map) {

	for (auto &r : map) {
		for (auto &k : *r.second) {
			std::string transaction = _formatTransactionString(r.first, k.first);

			for (auto &p : _equivalentRates) {
				if (p.transaction == transaction) {
					p.standardRate = k.second;
				}
			}
		}
	}
}

std::string ForexAnalyzer::_formatTransactionString(const std::string &first, const std::string &second) {
	return first + "_to_" + second;
}

double ForexAnalyzer::_doTransaction(double const first, double const second) {
	return first * second;
}

// returns 0 when all values are same
uint32_t ForexAnalyzer::_unequalEquivalentTransactions(const std::vector<Path>& paths) {

	for (uint32_t i = 0; i < paths.size() - 1; ++i) {
		if (!AreConsideredEqual(paths[i].rate, paths[i + 1].rate))
			return i + 1;
	}

	return 0;
}

bool ForexAnalyzer::AreConsideredEqual(double const first, double const second) {
	return fabs(first - second) < _threshold;
}

void ForexAnalyzer::Stop() {
	if (_checkerThread.joinable())
		_checkerThread.join();

	_killthread = true;
}

void ForexAnalyzer::_logSomeStats() {
	std::string str = "Starting Forex Analyzer with the following values:";
	SysLog::Log(SysLog::SeverityLevel::Debug, str);

	bool const defaultDepth = _nbCurrencies - 1 == _maxDepth;
	std::string defaultDepthStr = defaultDepth ? "Default depth is being used" : "custom depth is being used";

	SysLog::Log(SysLog::SeverityLevel::Debug, "N (nb currencies in the world) = " + std::to_string(NbCurrenciesInTheWorld));
	SysLog::Log(SysLog::SeverityLevel::Debug, "N (nb currencies current settings) = " + std::to_string(_nbCurrencies));
	SysLog::Log(SysLog::SeverityLevel::Debug, "MaxDepth (nb max transaction from base currency) = " + std::to_string(_maxDepth));
	SysLog::Log(SysLog::SeverityLevel::Debug, defaultDepthStr);
	SysLog::Log(SysLog::SeverityLevel::Debug, "N(N-1)/2 nb equivalent transaction = " + std::to_string(_nbCurrencies*(_nbCurrencies - 1)/2));
	
	SysLog::Log(SysLog::SeverityLevel::Debug, "!N (Nb possibile combinaison if default depth is used) = " + std::to_string(_factorial(_nbCurrencies)));
	SysLog::Log(SysLog::SeverityLevel::Debug, "Actual nb possible combinaison with depth = " + std::to_string(_nbPossibilites(_nbCurrencies)));

	SysLog::Log(SysLog::SeverityLevel::Debug, "Nb Operation to be done " + std::to_string(_nbOperations(_nbCurrencies)));
	SysLog::LogEmptyLine();
}

uint64_t ForexAnalyzer::_nbPossibilites(uint16_t nbCurrencies) {
	uint64_t poss = 1;

	for (uint16_t i = 0; i <= _maxDepth; ++i)
		poss *= (nbCurrencies - i);

	return poss;

}

uint64_t ForexAnalyzer::_nbOperations(uint16_t nbCurrencies) {
	return _nbPossibilites(nbCurrencies)*(_maxDepth - 1);
}

uint64_t ForexAnalyzer::_factorial(uint16_t val) {
	uint64_t mul = 1;

	for (uint16_t i = 1; i <= val ; ++i) {
		mul *= i;
	}

	return mul;
}