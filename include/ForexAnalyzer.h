#ifndef FOREXANALYZER_H
#define FOREXANALYZER_H
#include <include/CurrencyAPI.h>
#include <thread>
#include <vector>

constexpr uint16_t NbCurrenciesInTheWorld = 180;

struct Path {
	std::vector<std::string> fullPath;
	double rate;

	Path(std::string const &baseCurrency) {
		fullPath.emplace_back(baseCurrency);
		rate = 1;
	}

	void AddTransaction(std::string const &newCurrency, double const newRate) {
		fullPath.emplace_back(newCurrency);
		rate *= newRate;
	}

	Path() { memset(this, 0, sizeof(Path)); }
};

struct EquivalentTransactions {
	std::string transaction;
	std::vector<Path> Paths;
	double standardRate;

	void AddPath(Path &newPath) {
		Paths.emplace_back(newPath);
	}

	void AddTransaction(size_t const index,std::string &currency, double const rate) {
		Path &pathToUpdate = Paths.at(index);
		pathToUpdate.AddTransaction(currency, rate);
	}


	EquivalentTransactions(std::string transationName): transaction(transationName){
		
	}
};

class ForexAnalyzer {
public:
	ForexAnalyzer(json::JSON &cfg, std::shared_ptr<CurrencyAPI> currencyAPI);

	void _initTransactionMap();

	void Run();
	void Stop();

private:
	std::shared_ptr<CurrencyAPI> _currencyAPI;
	uint16_t _nbCurrencies;
	std::vector<std::string> _currencies;
	std::vector<std::thread> _threads;
	std::thread _checkerThread;
	std::chrono::milliseconds _sleep;
	uint8_t _maxDepth;
	double _threshold;
	std::vector<EquivalentTransactions> _equivalentRates;
	bool _killthread;
	void _logSomeStats();
	uint64_t _nbPossibilites(uint16_t nbCurrencies);
	uint64_t _nbOperations(uint16_t nbCurrencies);
	void _checkAndCompute();
	void _analyzeNewData();
	void _processMap(RatesMap const & map);
	void _processAllTransactionPaths(EquivalentTransactions &eqTransaction, RatesMap const &map);
	void _fillPaths(EquivalentTransactions &eqTransac, uint16_t eqPerGroup);
	void _recursiveTransaction(Path &paths);
	uint64_t _factorial(uint16_t val);
	void _appendBasicRate(RatesMap const &map);
	std::string _formatTransactionString(const std::string &first, const std::string &second);
	void _processTransactions(RatesMap const &Map);
	double _doTransaction(double const first, double const second);
	uint32_t _unequalEquivalentTransactions(const std::vector<Path> &paths);
	bool AreConsideredEqual(double const first, double const second);
};

#endif // !FOREXANALYZER_H
